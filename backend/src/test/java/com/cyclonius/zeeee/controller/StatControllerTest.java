package com.cyclonius.zeeee.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StatControllerTest {

    @Autowired
    private StatController statController;

    private MockMvc mockMvc;
    private MockHttpSession session;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(statController).build();
        session = new MockHttpSession();
        session.setAttribute("userId",1);
    }

    @Test
    public void getUserStatTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/stat/get_user_stat")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .session(session))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(
                        "{\"state\":200,\"message\":null,\"data\":{\"cntRecent\":[],\"cntTotal\":0}}"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
