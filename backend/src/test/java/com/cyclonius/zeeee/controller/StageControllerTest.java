package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.dao.StageDao;
import com.cyclonius.zeeee.dao.TextDao;
import com.cyclonius.zeeee.dao.UserStageDao;
import com.cyclonius.zeeee.domain.Stage;
import com.cyclonius.zeeee.domain.Text;
import com.cyclonius.zeeee.domain.UserStage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StageControllerTest {
    @Autowired
    private StageController stageController;

    private MockMvc mockMvc;
    private MockHttpSession session;
    @Autowired
    private StageDao stageDao;
    @Autowired
    private UserStageDao userStageDao;
    @Autowired
    private TextDao textDao;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(stageController).build();
        session = new MockHttpSession();
        session.setAttribute("userId",1);
        if (textDao.findByTextId(1) == null) {
            Text text = new Text();
            text.setTextName("test_text_1");
            text.setTextContent("内容：test_text_content_1");
            text.setTextType("test_type");
            textDao.insertText(text);
        }
        if (stageDao.findByStageId(1) == null) {
            Stage stage = new Stage();
            stage.setTextId(1);
            stage.setStageType("test_type");
            stageDao.insertStage(stage);
        }
        if (userStageDao.getStageStarByUserIdAndStageId(1,1) == null) {
            UserStage userStage = new UserStage();
            userStage.setStageId(1);
            userStage.setUserId(1);
            userStage.setStageStar(-1);
            userStageDao.insertUS(userStage);
        }
    }

    @Test
    public void getStageTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/stage/get_stages")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .session(session))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(
                        "{\"state\":200,\"message\":null,\"data\":[{\"stageId\":1,\"stageType\":\"test_type\",\"textName\":\"test_text_1\",\"stageStar\":-1}]}"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
