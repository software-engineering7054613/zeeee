package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.dao.TextDao;
import com.cyclonius.zeeee.domain.Text;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TextControllerTest {

    @Autowired
    private TextController textController;

    private MockMvc mockMvc;
    private MockHttpSession session;
    @Autowired
    private TextDao textDao;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(textController).build();
        session = new MockHttpSession();
        session.setAttribute("userId",1);
        if (textDao.findByTextName("test_text_1") == null) {
            Text text = new Text();
            text.setTextName("test_text_1");
            text.setTextContent("内容：test_text_content_1");
            text.setTextType("test_type");
            textDao.insertText(text);
        }
    }

    @Test
    public void getTextTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(
                "/text/get_text/{text_name}", "test_text_1")
                        .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(
                        "{\"state\":200,\"message\":null,\"data\":\"内容：test_text_content_1\"}"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
