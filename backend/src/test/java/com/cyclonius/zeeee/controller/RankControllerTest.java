package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.dao.RankContentDao;
import com.cyclonius.zeeee.domain.RankContent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Calendar;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RankControllerTest {
    @Autowired
    private RankController rankController;

    private MockMvc mockMvc;
    private MockHttpSession session;

    @Autowired
    private RankContentDao rankContentDao;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(rankController).build();
        session = new MockHttpSession();
        session.setAttribute("userId", 1);
        Calendar calendar = Calendar.getInstance();
        Integer week = calendar.get(Calendar.WEEK_OF_YEAR);
        if (rankContentDao.getTextIdByWeek(week) == null) {
            RankContent rankContent = new RankContent();
            rankContent.setTextId(1);
            rankContent.setWeek(week);
            rankContentDao.insertRC(rankContent);
        }
    }

    @Test
    public void startRankTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/rank/start_rank")
                        .session(session)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void finishRankTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/rank/finish_rank")
                        .session(session)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"time\": 7500}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void getRankTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/rank/get_rank")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .session(session))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
