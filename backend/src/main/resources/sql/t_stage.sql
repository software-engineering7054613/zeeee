CREATE DATABASE IF NOT EXISTS zeeee character SET utf8;

CREATE TABLE t_stage (
                        stage_id INT AUTO_INCREMENT COMMENT '编号',
                        stage_type VARCHAR(10) NOT NULL COMMENT '类型',
                        text_id INT NOT NULL UNIQUE COMMENT '文章id',
                        PRIMARY KEY (stage_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;