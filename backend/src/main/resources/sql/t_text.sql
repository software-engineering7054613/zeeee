CREATE DATABASE IF NOT EXISTS zeeee character SET utf8;

CREATE TABLE t_text
(
    text_id      INT AUTO_INCREMENT COMMENT '文章id',
    text_name    VARCHAR(20) NOT NULL COMMENT '篇名',
    text_content TEXT        NOT NULL COMMENT '内容',
    text_type    VARCHAR(10) COMMENT '类型',
    PRIMARY KEY (text_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;