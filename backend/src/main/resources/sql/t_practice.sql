CREATE DATABASE IF NOT EXISTS zeeee character SET utf8;

CREATE TABLE t_practice (
                        prac_id INT AUTO_INCREMENT COMMENT '练习id',
                        user_id INT NOT NULL COMMENT '用户id',
                        text_id INT NOT NULL COMMENT '文章id',
                        start_time datetime COMMENT '开始时间',
                        PRIMARY KEY (prac_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;