CREATE DATABASE IF NOT EXISTS zeeee character SET utf8;

CREATE TABLE t_user (
                        user_id INT AUTO_INCREMENT COMMENT '用户id',
                        user_name VARCHAR(20) NOT NULL UNIQUE COMMENT '用户名',
                        password VARCHAR(40) NOT NULL COMMENT '密码',
                        salt CHAR(36) COMMENT '盐值',
                        PRIMARY KEY (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;