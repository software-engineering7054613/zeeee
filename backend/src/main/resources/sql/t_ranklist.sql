CREATE DATABASE IF NOT EXISTS zeeee character SET utf8;

CREATE TABLE t_ranklist (
                        rl_id INT AUTO_INCREMENT COMMENT '排位项id',
                        week int NOT NULL COMMENT '周号',
                        user_id int NOT NULL COMMENT '用户id',
                        min_time bigint COMMENT '用户最短用时',
                        PRIMARY KEY (rl_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;