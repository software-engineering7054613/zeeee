CREATE DATABASE IF NOT EXISTS zeeee character SET utf8;

CREATE TABLE t_rank (
                        rank_id INT AUTO_INCREMENT COMMENT '排位id',
                        user_id INT NOT NULL COMMENT '用户id',
                        start_time datetime COMMENT '开始时间',
                        PRIMARY KEY (rank_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;