CREATE DATABASE IF NOT EXISTS zeeee character SET utf8;

CREATE TABLE t_userstage (
                        us_id INT AUTO_INCREMENT COMMENT '关联id',
                        user_id INT NOT NULL COMMENT '用户id',
                        stage_id INT NOT NULL COMMENT '关卡编号',
                        stage_star INT DEFAULT -1 COMMENT '星级',
                        PRIMARY KEY (us_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;