CREATE DATABASE IF NOT EXISTS zeeee character SET utf8;

CREATE TABLE t_stat (
                        stat_id INT AUTO_INCREMENT COMMENT '数据id',
                        user_id INT NOT NULL COMMENT '用户id',
                        date DATE NOT NULL COMMENT '日期',
                        cnt INT DEFAULT 0 COMMENT '当日练习篇数',
                        PRIMARY KEY (stat_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;