CREATE DATABASE IF NOT EXISTS zeeee character SET utf8;

CREATE TABLE t_rankcontent (
                        week INT NOT NULL COMMENT '周号',
                        text_id INT NOT NULL COMMENT '文章id',
                        PRIMARY KEY (week)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;