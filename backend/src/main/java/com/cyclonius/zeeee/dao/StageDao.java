package com.cyclonius.zeeee.dao;

import com.cyclonius.zeeee.domain.Stage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理关卡数据操作的持久层接口
 */
@Repository
public interface StageDao {
    /**
     * 插入关卡数据
     *
     * @param stage 用户数据
     * @return 受影响的行数
     */
    Integer insertStage(Stage stage);

    /**
     * 获取全部关卡id
     *
     * @return 全部关卡id，如果没有关卡数据，则返回null
     */
    List<Integer> getAllStageIds();

    /**
     * 根据编号查询关卡数据
     *
     * @param stageId 编号
     * @return 匹配的关卡数据，如果没有匹配的数据，则返回null
     */
    Stage findByStageId(Integer stageId);

    /**
     * 根据关卡id查询关卡数据
     *
     * @param textId 关卡id
     * @return 匹配的关卡数据，如果没有匹配的数据，则返回null
     */
    Stage findByTextId(Integer textId);

    /**
     * 根据类型查询关卡数据
     *
     * @param stageType 类型
     * @return 匹配的关卡数据，如果没有匹配的数据，则返回null
     */
    List<Stage> findByStageType(String stageType);

    /**
     * 根据编号更新类型
     *
     * @param stageId 编号
     * @param stageType 类型
     * @return 受影响的行数
     */
    Integer updateStageTypeByStageId(
            @Param("stageId") Integer stageId,
            @Param("stageType") String stageType);
}
