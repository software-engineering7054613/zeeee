package com.cyclonius.zeeee.dao;

import com.cyclonius.zeeee.domain.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 处理用户数据操作的持久层接口
 */
@Repository
public interface UserDao {
    /**
     * 插入用户数据
     *
     * @param user 用户数据
     * @return 受影响的行数
     */
    Integer insertUser(User user);

    /**
     * 根据用户名查询用户数据
     *
     * @param userName 用户名
     * @return 匹配的用户数据，如果没有匹配的数据，则返回null
     */
    User findByUserName(String userName);

    /**
     * 根据uid更新用户的密码
     *
     * @param userId   用户的id
     * @param password 新密码
     * @return 受影响的行数
     */
    Integer updatePasswordByUserId(
            @Param("userId") Integer userId,
            @Param("password") String password);

    /**
     * 根据uid更新用户的密码
     *
     * @param userId   用户的id
     * @param userName 新密码
     * @return 受影响的行数
     */
    Integer updateUserNameByUserId(
            @Param("userId") Integer userId,
            @Param("userName") String userName);

    /**
     * 根据用户id查询用户数据
     *
     * @param userId 用户id
     * @return 匹配的用户数据，如果没有匹配的用户数据，则返回null
     */
    User findByUserId(Integer userId);

    /**
     * 根据uid更新用户资料
     *
     * @param user 封装了用户id和新个人资料的对象
     * @return 受影响的行数
     */
    Integer updateInfoByUid(User user);
}
