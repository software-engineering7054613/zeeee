package com.cyclonius.zeeee.dao;

import com.cyclonius.zeeee.domain.Practice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 处理练习数据操作的持久层接口
 */
@Repository
public interface PracticeDao {
    /**
     * 插入练习数据
     *
     * @param practice 练习数据
     * @return 受影响的行数
     */
    Integer insertPractice(Practice practice);

    /**
     * 根据用户id和文章id查询练习数据
     *
     * @param userId 用户id
     * @param textId 文章id
     * @return 匹配的练习数据，如果没有匹配的数据，则返回null
     */
    Practice findByUserIdAndTextId(
            @Param("userId") Integer userId,
            @Param("textId") Integer textId);

    /**
     * 根据练习id删除练习数据
     *
     * @param pracId 练习id
     * @return 受影响的行数
     */
    Integer deleteByPracId(Integer pracId);
}
