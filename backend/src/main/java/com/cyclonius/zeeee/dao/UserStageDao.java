package com.cyclonius.zeeee.dao;

import com.cyclonius.zeeee.domain.UserStage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理用户关卡关联操作的持久层接口
 */
@Repository
public interface UserStageDao {
    /**
     * 插入用户关卡关联
     *
     * @param userStage 用户关卡关联
     * @return 受影响的行数
     */
    Integer insertUS(UserStage userStage);

    /**
     * 根据用户id查询用户关卡关联
     *
     * @param userId 用户id
     * @return 匹配的用户关卡关联，如果没有匹配的数据，则返回null
     */
    List<UserStage> findByUserId(Integer userId);

    /**
     * 根据用户id和关卡id查询星级
     *
     * @param userId 用户id
     * @param stageId 关卡id
     * @return 匹配的星级，如果没有匹配的数据，则返回null
     */
    Integer getStageStarByUserIdAndStageId(
            @Param("userId") Integer userId,
            @Param("stageId") Integer stageId);

    /**
     * 根据用户id和关卡id更新星级
     *
     * @param userId 用户id
     * @param stageId 关卡id
     * @param stageStar 星级
     * @return 受影响的行数
     */
    Integer updateStageStarByUserIdAndStageId(
            @Param("userId") Integer userId,
            @Param("stageId") Integer stageId,
            @Param("stageStar") Integer stageStar);
}
