package com.cyclonius.zeeee.dao;

import com.cyclonius.zeeee.domain.RankList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理每周排位数据操作的持久层接口
 */
@Repository
public interface RankListDao {
    /**
     * 插入每周排位数据
     *
     * @param rankList 排位数据
     * @return 受影响的行数
     */
    Integer insertRL(RankList rankList);

    /**
     * 根据周号查询每周排位数据，按最短用时升序排列
     *
     * @param week 周号
     * @return 匹配的每周排位数据，升序排列，如果没有匹配的数据，则返回null
     */
    List<RankList> findByWeek(Integer week);

    /**
     * 根据用户id和周号查询最短用时
     *
     * @param userId 用户id
     * @param week   周号
     * @return 匹配的最短用时，如果没有匹配的数据，则返回null
     */
    Long getMinTimeByUserIdAndWeek(
            @Param("userId") Integer userId,
            @Param("week") Integer week);

    /**
     * 根据用户id和周号更新最短用时
     *
     * @param userId  用户id
     * @param week    周号
     * @param minTime 最短用时
     * @return 受影响的行数
     */
    Integer updateMinTimeByUserIdAndWeek(
            @Param("userId") Integer userId,
            @Param("week") Integer week,
            @Param("minTime") Long minTime);

    /**
     * 当周用时比minTime短的用户数量，+1得到周排名
     *
     * @param minTime 最短用时
     * @param week    周号
     * @return 当周用时比minTime短的用户数量
     */
    Integer getUserRankByMinTimeAndWeek(
            @Param("minTime") Long minTime,
            @Param("week") Integer week);
}
