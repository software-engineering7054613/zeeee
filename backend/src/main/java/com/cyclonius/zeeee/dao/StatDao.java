package com.cyclonius.zeeee.dao;

import com.cyclonius.zeeee.domain.Stat;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.sql.Date;

/**
 * 用户统计数据的持久层接口
 */
@Repository
public interface StatDao {
    /**
     * 插入用户统计数据
     *
     * @param stat 用户数据
     * @return 受影响的行数
     */
    Integer insertStat(Stat stat);

    /**
     * 根据数据id查询用户统计数据
     *
     * @param statId 数据id
     * @return 匹配的用户统计数据，如果没有匹配的数据，则返回null
     */
    Stat findByStatId(Integer statId);

    /**
     * 根据用户id查询用户统计数据
     *
     * @param userId 用户id
     * @return 匹配的用户统计数据，如果没有匹配的数据，则返回null
     */
    List<Stat> findByUserId(Integer userId);

    /**
     * 根据用户id和日期查询当日练习篇数
     *
     * @param userId 用户id
     * @param date 日期
     * @return 匹配的当日练习篇数，如果没有匹配的数据，则返回null
     */
    Integer getCntByUserIdAndDate(
            @Param("userId") Integer userId,
            @Param("date") Date date);

    /**
     * 根据用户id和日期更新当日练习篇数
     *
     * @param userId 用户id
     * @param date 日期
     * @param cnt 当日练习篇数
     * @return 受影响的行数
     */
    Integer updateCntByUserIdAndDate(
            @Param("userId") Integer userId,
            @Param("date") Date date,
            @Param("cnt") Integer cnt);
}
