package com.cyclonius.zeeee.dao;

import com.cyclonius.zeeee.domain.Rank;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 处理排位数据操作的持久层接口
 */
@Repository
public interface RankDao {
    /**
     * 插入排位数据
     *
     * @param rank 排位数据
     * @return 受影响的行数
     */
    Integer insertRank(Rank rank);

    /**
     * 根据用户id查询排位数据
     *
     * @param userId 用户id
     * @return 匹配的排位数据，如果没有匹配的数据，则返回null
     */
    Rank findByUserId(Integer userId);

    /**
     * 根据排位id删除排位数据
     *
     * @param rankId 排位id
     * @return 受影响的行数
     */
    Integer deleteByRankId(Integer rankId);
}
