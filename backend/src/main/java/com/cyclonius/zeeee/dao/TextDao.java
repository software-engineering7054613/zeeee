package com.cyclonius.zeeee.dao;

import com.cyclonius.zeeee.domain.Text;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理文章数据操作的持久层接口
 */
@Repository
public interface TextDao {
    /**
     * 插入文章数据
     *
     * @param text 文章数据
     * @return 受影响的行数
     */
    Integer insertText(Text text);

    /**
     * 根据篇名查询文章数据
     *
     * @param textName 篇名
     * @return 匹配的文章数据，如果没有匹配的数据，则返回null
     */
    Text findByTextName(String textName);

    /**
     * 根据文章id查询文章数据
     *
     * @param textId 文章id
     * @return 匹配的文章数据，如果没有匹配的数据，则返回null
     */
    Text findByTextId(Integer textId);

    /**
     * 根据类型查询文章数据
     *
     * @param textType 类型
     * @return 匹配的文章数据，如果没有匹配的数据，则返回null
     */
    List<Text> findByTextType(String textType);

    /**
     * 根据文章id更新类型
     *
     * @param textId 文章id
     * @return 受影响的行数
     */
    Integer updateTypeByTextId(Integer textId);

    /**
     * 返回全部文章列表
     * @return 所有文章数据
     */
    List<Text> getAllTexts();
}
