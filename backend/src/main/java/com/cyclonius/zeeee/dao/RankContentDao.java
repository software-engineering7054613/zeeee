package com.cyclonius.zeeee.dao;

import com.cyclonius.zeeee.domain.RankContent;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 处理每周排位文章操作的持久层接口
 */
@Repository
public interface RankContentDao {
    /**
     * 插入每周排位文章
     *
     * @param rankContent 每周排位文章
     * @return 受影响的行数
     */
    Integer insertRC(RankContent rankContent);

    /**
     * 根据周号查询每周排位文章id
     *
     * @param week 周号
     * @return 每周排位文章id，如果没有匹配的数据，则返回null
     */
    Integer getTextIdByWeek(Integer week);

    /**
     * 根据文章id查询周号
     *
     * @param textId 用户id
     * @return 匹配的周号，如果没有匹配的数据，则返回null
     */
    Integer getWeekByTextId(Integer textId);
}
