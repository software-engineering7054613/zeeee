package com.cyclonius.zeeee.domain;

import lombok.Data;

@Data
public class Text {
    private Integer textId;
    private String textName;
    private String textContent;
    private String textType;
}
