package com.cyclonius.zeeee.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Rank {
    private Integer rankId;
    private Integer userId;
    private LocalDateTime startTime;
}
