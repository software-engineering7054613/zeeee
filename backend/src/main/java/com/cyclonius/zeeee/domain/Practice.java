package com.cyclonius.zeeee.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Practice {
    private Integer pracId;
    private Integer userId;
    private Integer textId;
    private LocalDateTime startTime;
}
