package com.cyclonius.zeeee.domain;

import lombok.Data;

@Data
public class RankContent {
    private Integer week;
    private Integer textId;
}
