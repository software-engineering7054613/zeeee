package com.cyclonius.zeeee.domain;

import lombok.Data;

import java.sql.Date;

@Data
public class Stat {
    private Integer statId;
    private Integer userId;
    private Date date;
    private Integer cnt;
}
