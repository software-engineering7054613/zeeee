package com.cyclonius.zeeee.domain;

import lombok.Data;

@Data
public class UserStage {
    private Integer usId;
    private Integer userId;
    private Integer stageId;
    private Integer stageStar;
}


