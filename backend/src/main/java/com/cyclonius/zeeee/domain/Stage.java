package com.cyclonius.zeeee.domain;

import lombok.Data;

@Data
public class Stage {
    private Integer stageId;
    private String stageType;
    private Integer textId;
}
