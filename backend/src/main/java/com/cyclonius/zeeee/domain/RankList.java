package com.cyclonius.zeeee.domain;

import lombok.Data;

@Data
public class RankList {
    private Integer rlId;
    private Integer week;
    private Integer userId;
    private Long minTime;//单位毫秒
}
