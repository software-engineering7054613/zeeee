package com.cyclonius.zeeee.service;

import com.cyclonius.zeeee.dao.StatDao;
import com.cyclonius.zeeee.domain.Stat;
import com.cyclonius.zeeee.vo.CountByDateVO;
import com.cyclonius.zeeee.vo.UserStatVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StatService {
    @Autowired
    private StatDao statDao;

    public UserStatVO getUserStat(Integer userId) {
        List<Stat> stats = statDao.findByUserId(userId);
        UserStatVO userStatVO = new UserStatVO();
        List<CountByDateVO> list = new ArrayList<>();
        int total = 0;

        for (Stat stat : stats) {
            CountByDateVO vo = new CountByDateVO();
            total = total + stat.getCnt();
            vo.setCnt(stat.getCnt());
            vo.setDate(stat.getDate());
            list.add(vo);
        }
        userStatVO.setCntRecent(list);
        userStatVO.setCntTotal(total);
        return userStatVO;
    }
}
