package com.cyclonius.zeeee.service;

import com.cyclonius.zeeee.dao.StageDao;
import com.cyclonius.zeeee.dao.TextDao;
import com.cyclonius.zeeee.dao.UserStageDao;
import com.cyclonius.zeeee.domain.Stage;
import com.cyclonius.zeeee.domain.UserStage;
import com.cyclonius.zeeee.vo.StageInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StageService {
    @Autowired
    private StageDao stageDao;
    @Autowired
    private UserStageDao userStageDao;
    @Autowired
    private TextDao textDao;

    public List<StageInfoVO> getStagesInfo(Integer userId) {
        List<UserStage> userStages = userStageDao.findByUserId(userId);
        List<StageInfoVO> stageInfoVOList = new ArrayList<>();
        for (UserStage userStage : userStages) {
            StageInfoVO stageInfoVO = new StageInfoVO();
            Integer stageId = userStage.getStageId();
            Integer stageStar = userStage.getStageStar();
            Stage stage = stageDao.findByStageId(stageId);
            String stageType = stage.getStageType();
            Integer textId = stage.getTextId();
            String textName = textDao.findByTextId(textId).getTextName();
            stageInfoVO.setStageId(stageId);
            stageInfoVO.setStageStar(stageStar);
            stageInfoVO.setStageType(stageType);
            stageInfoVO.setTextName(textName);
            stageInfoVOList.add(stageInfoVO);
        }
        return stageInfoVOList;
    }
}
