package com.cyclonius.zeeee.service.ex;

public class PracticeNotFoundException extends ServiceException {
    public PracticeNotFoundException() {
        super();
    }

    public PracticeNotFoundException(String message) {
        super(message);
    }

    public PracticeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PracticeNotFoundException(Throwable cause) {
        super(cause);
    }

    protected PracticeNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
