package com.cyclonius.zeeee.service.ex;

public class RankDuplicateException extends ServiceException {
    public RankDuplicateException() {
        super();
    }

    public RankDuplicateException(String message) {
        super(message);
    }

    public RankDuplicateException(String message, Throwable cause) {
        super(message, cause);
    }

    public RankDuplicateException(Throwable cause) {
        super(cause);
    }

    protected RankDuplicateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
