package com.cyclonius.zeeee.service;

import com.cyclonius.zeeee.dao.*;
import com.cyclonius.zeeee.domain.Practice;
import com.cyclonius.zeeee.domain.Stat;
import com.cyclonius.zeeee.service.ex.PracticeDuplicateException;
import com.cyclonius.zeeee.service.ex.PracticeNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
public class PracticeService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private TextDao textDao;
    @Autowired
    private PracticeDao practiceDao;
    @Autowired
    private StatDao statDao;
    @Autowired
    private StageDao stageDao;
    @Autowired
    private UserStageDao userStageDao;

    public void startPractice(Integer userId, String textName) {
        Practice practice = new Practice();
        practice.setPracId(null);
        practice.setUserId(userId);
        Integer textId = textDao.findByTextName(textName).getTextId();
        practice.setTextId(textId);
        practice.setStartTime(LocalDateTime.now());
        if (practiceDao.findByUserIdAndTextId(userId, textId) != null) {
            throw new PracticeDuplicateException("该文章练习正在进行中");
        }
        practiceDao.insertPractice(practice);
    }

    public void finishPractice(Integer userId, String textName, Long time) {
        Integer textId = textDao.findByTextName(textName).getTextId();

        float speed = textDao.findByTextId(textId).getTextContent().length() / ((float) time / 60000);
        //System.out.println(speed);
        Integer stageId = stageDao.findByTextId(textId).getStageId();
        Integer stageStar = userStageDao.getStageStarByUserIdAndStageId(userId, stageId);
        if (stageStar == 0 && speed >= 120) {
            for (int newId = stageId + 1; stageDao.findByStageId(newId) != null; newId ++) {
                if (userStageDao.getStageStarByUserIdAndStageId(userId, newId) == -1) {
                    userStageDao.updateStageStarByUserIdAndStageId(userId, newId, 0);
                    break;
                }
            }
        }
        if(stageStar < 3 && speed >= 180) {     //更新用户关卡星级
            userStageDao.updateStageStarByUserIdAndStageId(userId, stageId, 3);
        } else if(stageStar < 2 && speed >= 150) {     //更新用户关卡星级
            userStageDao.updateStageStarByUserIdAndStageId(userId, stageId, 2);
        } else if(stageStar < 1 && speed >= 120) {     //更新用户关卡星级
            userStageDao.updateStageStarByUserIdAndStageId(userId, stageId, 1);
        }

        LocalDate date = LocalDate.now();
        Integer cnt = statDao.getCntByUserIdAndDate(userId, java.sql.Date.valueOf(date));
        if (cnt == null) {
            Stat stat = new Stat();
            stat.setStatId(null);
            stat.setUserId(userId);
            stat.setDate(java.sql.Date.valueOf(date));
            stat.setCnt(1);
            statDao.insertStat(stat);    //添加stat
        } else {
            statDao.updateCntByUserIdAndDate(userId, java.sql.Date.valueOf(date), cnt + 1);      //更新stat的cnt
        }
    }
}
