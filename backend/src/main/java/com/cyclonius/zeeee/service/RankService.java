package com.cyclonius.zeeee.service;

import com.cyclonius.zeeee.dao.*;
import com.cyclonius.zeeee.domain.*;
import com.cyclonius.zeeee.service.ex.*;
import com.cyclonius.zeeee.vo.RankVO;
import com.cyclonius.zeeee.vo.Top3VO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@Service
public class RankService {
    @Autowired
    private RankListDao rankListDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RankDao rankDao;
    @Autowired
    private RankContentDao rankContentDao;
    @Autowired
    private TextDao textDao;

    public RankVO getRank(Integer userId) {
        RankVO rankVO = new RankVO();
        Calendar calendar = Calendar.getInstance();
        Integer week = calendar.get(Calendar.WEEK_OF_YEAR);
        List<RankList> rankLists = rankListDao.findByWeek(week);
        Long minTime = rankListDao.getMinTimeByUserIdAndWeek(userId, week);
        Integer rank = rankListDao.getUserRankByMinTimeAndWeek(minTime, week) + 1;
        Integer rankTextId = rankContentDao.getTextIdByWeek(week);
        String rankTextName = textDao.findByTextId(rankTextId).getTextName();
        List<Top3VO> top3VOList = new ArrayList<>();
        Set<Integer> topSet = new HashSet<>();
        for (RankList rankList : rankLists) {
            Integer uid = rankList.getUserId();
            if (!topSet.contains(uid)) {
                Top3VO top3VO = new Top3VO();
                String uname = userDao.findByUserId(userId).getUserName();
                top3VO.setUserName(uname);
                top3VO.setMinTime(rankList.getMinTime());
                top3VOList.add(top3VO);
                topSet.add(uid);
            }
            if (top3VOList.size() >= 3) {
                break;
            }
        }
        rankVO.setMinTime(minTime);
        rankVO.setRank(rank);
        rankVO.setRankTextName(rankTextName);
        rankVO.setTop3(top3VOList);
        return rankVO;
    }

    public void startRank(Integer userId) {
        Rank rank = new Rank();
        rank.setRankId(null);
        rank.setUserId(userId);
        rank.setStartTime(LocalDateTime.now());
        if (rankDao.findByUserId(userId) != null) {
            throw new RankDuplicateException("排位正在进行中");
        }
        rankDao.insertRank(rank);
    }

    public void finishRank(Integer userId, Long time) {
        Calendar calendar = Calendar.getInstance();
        Integer week = calendar.get(Calendar.WEEK_OF_YEAR);     //获取周号

        Long formerMinTime = rankListDao.getMinTimeByUserIdAndWeek(userId, week);
        if (formerMinTime == null) {
            RankList rankList = new RankList();
            rankList.setUserId(userId);
            rankList.setWeek(week);
            rankList.setMinTime(time);
            rankListDao.insertRL(rankList);     //添加rankList
        } else if(time < formerMinTime) {
            rankListDao.updateMinTimeByUserIdAndWeek(userId, week, time);      //更新rankList的minTime
        }
    }
}
