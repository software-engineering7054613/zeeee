package com.cyclonius.zeeee.service;

import com.cyclonius.zeeee.dao.TextDao;
import com.cyclonius.zeeee.domain.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TextService {
    @Autowired
    private TextDao textDao;

    public String getText(String textName) {
        String content = textDao.findByTextName(textName).getTextContent();
        return content;
    }

    public List<Text> getAllTexts() {
        List<Text> texts = textDao.getAllTexts();
        return texts;
    }
}
