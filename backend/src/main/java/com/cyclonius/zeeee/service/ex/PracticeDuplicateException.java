package com.cyclonius.zeeee.service.ex;

/** 练习重复异常 */
public class PracticeDuplicateException extends ServiceException {
    public PracticeDuplicateException() {
        super();
    }

    public PracticeDuplicateException(String message) {
        super(message);
    }

    public PracticeDuplicateException(String message, Throwable cause) {
        super(message, cause);
    }

    public PracticeDuplicateException(Throwable cause) {
        super(cause);
    }

    protected PracticeDuplicateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
