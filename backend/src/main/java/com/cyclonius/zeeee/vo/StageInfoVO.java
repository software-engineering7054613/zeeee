package com.cyclonius.zeeee.vo;

import lombok.Data;

@Data
public class StageInfoVO {
    private Integer stageId;
    private String stageType;
    private String textName;
    private Integer stageStar;
}
