package com.cyclonius.zeeee.vo;

import lombok.Data;

import java.util.Date;

@Data
public class CountByDateVO {
    private Date date;
    private Integer cnt;
}
