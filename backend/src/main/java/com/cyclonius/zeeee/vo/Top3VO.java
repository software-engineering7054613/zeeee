package com.cyclonius.zeeee.vo;

import lombok.Data;

@Data
public class Top3VO {
    private String userName;
    private Long minTime;
}
