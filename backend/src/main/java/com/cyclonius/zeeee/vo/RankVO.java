package com.cyclonius.zeeee.vo;

import lombok.Data;

import java.util.List;

@Data
public class RankVO {
    private Long minTime;
    private Integer rank;
    private String rankTextName;
    private List<Top3VO> top3;
}
