package com.cyclonius.zeeee.vo;

import lombok.Data;

import java.util.List;

@Data
public class UserStatVO {
    private List<CountByDateVO> cntRecent;
    private Integer cntTotal;
}
