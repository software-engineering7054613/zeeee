package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.service.StageService;
import com.cyclonius.zeeee.util.JsonResult;
import com.cyclonius.zeeee.vo.StageInfoVO;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/stage")
public class StageController extends BaseController {
    @Autowired
    private StageService stageService;

    @GetMapping("/get_stages")
    public JsonResult<List<StageInfoVO>> getStages(HttpSession httpSession) {
        Integer userId = getUserIdFromSession(httpSession);
        List<StageInfoVO> stageInfoVOList = stageService.getStagesInfo(userId);
        return new JsonResult<>(OK, stageInfoVOList);
    }

}
