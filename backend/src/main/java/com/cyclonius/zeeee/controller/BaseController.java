package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.service.ex.*;
import com.cyclonius.zeeee.util.JsonResult;
import com.cyclonius.zeeee.controller.ex.ControlException;
import jakarta.servlet.http.HttpSession;
import org.springframework.web.bind.annotation.ExceptionHandler;


/**
 * 控制器类的基类
 */
public class BaseController {
    /**
     * 操作成功的状态码
     */
    public static final int OK = 200;

    /**
     * 从HttpSession对象中获取uid
     *
     * @param session HttpSession对象
     * @return 当前登录的用户的id
     */
    protected final Integer getUserIdFromSession(HttpSession session) {
        return Integer.valueOf(session.getAttribute("userId").toString());
    }

    /**
     * 从HttpSession对象中获取用户名
     *
     * @param session HttpSession对象
     * @return 当前登录的用户名
     */
    protected final String getUserNameFromSession(HttpSession session) {
        return session.getAttribute("userName").toString();
    }

    /**
     * @ExceptionHandler用于统一处理方法抛出的异常
     */
    @ExceptionHandler({ServiceException.class, ControlException.class})
    public JsonResult<Void> handleException(Throwable e) {
        JsonResult<Void> result = new JsonResult<Void>(e);
        if (e instanceof UserNameDuplicateException) {
            result.setState(4001);
        } else if (e instanceof UserNotFoundException) {
            result.setState(4002);
        } else if (e instanceof PasswordNotMatchException) {
            result.setState(4003);
        } else if (e instanceof InsertException) {
            result.setState(5000);
        } else if (e instanceof UpdateException) {
            result.setState(5001);
        } else if (e instanceof DeleteException) {
            result.setState(5002);
        } else if (e instanceof PracticeDuplicateException) {
            result.setState(6001);
        } else if (e instanceof PracticeNotFoundException) {
            result.setState(6002);
        } else if (e instanceof RankDuplicateException) {
            result.setState(7001);
        } else if (e instanceof RankNotFoundException) {
            result.setState(7002);
        }
        return result;
    }
}
