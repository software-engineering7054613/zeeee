package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.service.StatService;
import com.cyclonius.zeeee.util.JsonResult;
import com.cyclonius.zeeee.vo.RankVO;
import com.cyclonius.zeeee.vo.UserStatVO;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stat")
public class StatController extends BaseController {
    @Autowired
    private StatService statService;

    @GetMapping("/get_user_stat")
    private JsonResult<UserStatVO> getRank(HttpSession session) {
        Integer userId = getUserIdFromSession(session);
        UserStatVO data = statService.getUserStat(userId);
        return new JsonResult<>(OK, data);
    }
}
