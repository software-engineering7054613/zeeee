package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.domain.Text;
import com.cyclonius.zeeee.service.TextService;
import com.cyclonius.zeeee.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/text")
public class TextController extends BaseController {
    @Autowired
    private TextService textService;

    @GetMapping("/get_text/{text_name}")
    public JsonResult<String> getText(@PathVariable("text_name") String textName) {
        String data = textService.getText(textName);
        return new JsonResult<>(OK, data);
    }

    @GetMapping("/get_all_texts")
    public JsonResult<List<Text>> getAllTexts() {
        List<Text> data = textService.getAllTexts();
        return new JsonResult<>(OK, data);
    }
}
