package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.util.JsonResult;
import com.cyclonius.zeeee.domain.User;
import com.cyclonius.zeeee.service.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public JsonResult<Void> reg(@RequestBody User user) {
        // 调用业务对象执行注册
//        System.out.println(user);
        userService.reg(user);
        // 返回
        return new JsonResult<Void>(OK);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public JsonResult<User> login(@RequestBody Map<String, Object> map, HttpSession session) {
        String userName = (String) map.get("userName");
        String password = (String) map.get("password");

//        System.out.println(userName);
//        System.out.println(password);

        // 调用业务对象的方法执行登录，并获取返回值
        User data = userService.login(userName, password);
        //登录成功后，将uid和username存入到HttpSession中
        session.setAttribute("userId", data.getUserId());
        session.setAttribute("userName", data.getUserName());
        // System.out.println("Session中的uid=" + getUserIdFromSession(session));
        // System.out.println("Session中的username=" + getUserNameFromSession(session));

        // 将以上返回值和状态码OK封装到响应结果中并返回
        return new JsonResult<User>(OK, data);
    }

    @RequestMapping(value = "/change_password", method = RequestMethod.POST)
    public JsonResult<Void> changePassword(@RequestBody Map<String, Object> map, HttpSession session) {
        String oldPassword = (String) map.get("oldPassword");
        String newPassword = (String) map.get("newPassword");
        // 调用session.getAttribute("")获取uid和username
        Integer userId = getUserIdFromSession(session);

        // 调用业务对象执行修改密码
        userService.changePassword(userId, oldPassword, newPassword);
        // 返回成功
        return new JsonResult<Void>(OK);
    }

    @GetMapping(value = "/get_by_userId")
    public JsonResult<User> getByUserId(HttpSession session) {
        // 从HttpSession对象中获取uid
        Integer userId = getUserIdFromSession(session);
        // 调用业务对象执行获取数据
        User data = userService.getByUserId(userId);
        // 响应成功和数据
        return new JsonResult<User>(OK, data);
    }
}
