package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.domain.Rank;
import com.cyclonius.zeeee.domain.User;
import com.cyclonius.zeeee.service.RankService;
import com.cyclonius.zeeee.util.JsonResult;
import com.cyclonius.zeeee.vo.RankVO;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/rank")
public class RankController extends BaseController {
    @Autowired
    private RankService rankService;

    @GetMapping("/get_rank")
    public JsonResult<RankVO> getRank(HttpSession session) {
        Integer userId = getUserIdFromSession(session);
        RankVO data = rankService.getRank(userId);
        return new JsonResult<>(OK, data);
    }

    @RequestMapping(value = "/start_rank", method = RequestMethod.POST)
    public JsonResult<Void> startRank(HttpSession session) {
        Integer userId = getUserIdFromSession(session);
        rankService.startRank(userId);
        return new JsonResult<Void>(OK);
    }

    @RequestMapping(value = "/finish_rank", method = RequestMethod.POST)
    public JsonResult<Void> finishRank(@RequestBody Map<String, Object> map, HttpSession session) {
        Integer userId = getUserIdFromSession(session);
        Long time = Long.valueOf((Integer) map.get("time"));
        rankService.finishRank(userId, time);
        return new JsonResult<Void>(OK);
    }
}
