package com.cyclonius.zeeee.controller;

import com.cyclonius.zeeee.service.PracticeService;
import com.cyclonius.zeeee.util.JsonResult;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/prac")
public class PracticeController extends BaseController {
    @Autowired
    private PracticeService practiceService;

    @RequestMapping(value = "/start_practice", method = RequestMethod.POST)
    public JsonResult<Void> startPractice(@RequestBody Map<String, Object> map, HttpSession session) {
        Integer userId = getUserIdFromSession(session);
        String textName = (String) map.get("text_name");
        practiceService.startPractice(userId, textName);
        return new JsonResult<Void>(OK);
    }

    @RequestMapping(value = "/finish_practice", method = RequestMethod.POST)
    public JsonResult<Void> finishPractice(@RequestBody Map<String, Object> map, HttpSession session) {
        Integer userId = getUserIdFromSession(session);
        String textName = (String) map.get("text_name");
        Long time = Long.valueOf((Integer) map.get("time"));
        practiceService.finishPractice(userId, textName, time);
        return new JsonResult<Void>(OK);
    }
}
