import {Get,Post,Delete} from "@/api/request";

export default {
    Get: (url,params) => {
        return Get(url,params);
    },
    Post: (url,params) => {
        return Post(url,params);
    },
    Delete: (url,params) => {
        return Delete(url,params);
    }
}
