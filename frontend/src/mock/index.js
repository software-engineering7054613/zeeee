const Mock = require("mockjs");

Mock.mock('http://localhost:8081/user/login', 'post', (data) => {
    console.log(data)
    let a=JSON.parse(data.body).userName
    let b=JSON.parse(data.body).password
    console.log(a,b)
    if(a===b){
        return {
            status:200,
        }
    }else{
        return {state: 201}
    }
})

Mock.mock('http://localhost:8080/prac/finish_practice', 'post', (data) => {
    console.log(data)
})

Mock.mock('http://localhost:8080/rank/get_rank', 'get', {
    "state": 200,
    "message": null,
    "data": {
        "minTime": 11,
        "rank": 1,
        "rankTextName": "hello",
        "top3": [
            {
                "userName": "张三",
                "minTime": 11
            },
        ]
    }
})

Mock.mock('/stat/get_user_stat', 'get', {
    "state": 200,
    "message": null,
    "data": {
        "cntRecent": [
            {
                "date": "2023-04-19",
                "cnt": 2
            },
            {
                "date": "2023-04-20",
                "cnt": 2
            },
            {
                "date": "2023-04-21",
                "cnt": 2
            },
            {
                "date": "2023-04-22",
                "cnt": 100
            },
        ],
        "cntTotal": 6
    }
})

Mock.mock('/user/get_prac', 'get', {
    "state": 200,
    "message": null,
    "data": {
        "cntRecent": [
            {
                "date": "2023-04-19",
                "cnt": 2
            },
            {
                "date": "2023-04-20",
                "cnt": 2
            },
            {
                "date": "2023-04-21",
                "cnt": 2
            },
            {
                "date": "2023-04-22",
                "cnt": 100
            },
        ],
        "cntTotal": 6
    }
})

Mock.mock('http://localhost:8080/stage/get_stages', 'get', () => {
    return {data:[{
        stageId:10,
        textName:"niHao",
        stageStar:2,
    },{
            stageId:11,
            textName:"Hao",
            stageStar:2,}
        ]}
})

Mock.mock('http://localhost:8080/text/get_text', 'post', () => {
    return {data:"nisnaisna,snians,sniasn,snai"}
})