import Vue from 'vue'
import Router from 'vue-router'
import HomepageView from "@/components/Homepage/Homepage-view.vue";
import TypeView from "@/components/Type/Type-view.vue";
import PracticeSettle from "@/components/PracticeSettle.vue";
import RankSettle from "@/components/RankSettle.vue";
import UserStat from "@/components/UserStat.vue";

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            component: HomepageView
        },{
            path: '/homepage',
            component: HomepageView
        },{
            path: '/type',
            name:'typeview',
            component: TypeView
        },{
            path: '/prac',
			name:'practice',
            component: PracticeSettle
        },{
            path: '/rank',
            name:'rank',
            component: RankSettle
        },{
            path: '/user',
            name:'stat',
            component: UserStat
        },


    ],
    // mode: 'history'
})
