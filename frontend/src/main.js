import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router'
// import axios from 'axios'
import Api from "@/api";
// import "./mock/index"

import VueCookies from 'vue-cookies'
Vue.use(VueCookies)
Vue.prototype.$axios = Api

Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
